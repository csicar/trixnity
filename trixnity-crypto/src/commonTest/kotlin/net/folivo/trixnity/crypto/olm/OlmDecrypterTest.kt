package net.folivo.trixnity.crypto.olm

import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.collections.shouldHaveSize
import net.folivo.trixnity.core.model.UserId
import net.folivo.trixnity.core.model.events.ClientEvent.ToDeviceEvent
import net.folivo.trixnity.core.model.events.DecryptedOlmEvent
import net.folivo.trixnity.core.model.events.m.room.EncryptedEventContent
import net.folivo.trixnity.core.model.events.m.room.RoomMessageEventContent.TextMessageEventContent
import net.folivo.trixnity.core.model.keys.Key
import net.folivo.trixnity.core.model.keys.keysOf
import net.folivo.trixnity.crypto.mocks.OlmEncryptionServiceMock
import net.folivo.trixnity.olm.OlmLibraryException

class OlmDecrypterTest : ShouldSpec({
    timeout = 60_000

    lateinit var olmEncryptionServiceMock: OlmEncryptionServiceMock

    val subscriberReceived = mutableListOf<DecryptedOlmEventContainer>()
    val subscriber: DecryptedOlmEventSubscriber = {
        subscriberReceived.add(it)
    }

    lateinit var cut: OlmDecrypterImpl
    beforeTest {
        subscriberReceived.clear()
        olmEncryptionServiceMock = OlmEncryptionServiceMock()
        cut = OlmDecrypterImpl(olmEncryptionServiceMock)
        cut.subscribe(subscriber)
    }



    should("catch DecryptionException") {
        val event = ToDeviceEvent(
            EncryptedEventContent.OlmEncryptedEventContent(
                mapOf(), Key.Curve25519Key(null, "")
            ),
            UserId("sender", "server")
        )
        olmEncryptionServiceMock.decryptOlm = {
            throw DecryptionException.ValidationFailed("whoops")
        }
        cut.handleOlmEvent(event)
        subscriberReceived shouldHaveSize 0
    }
    should("catch KeyException") {
        val event = ToDeviceEvent(
            EncryptedEventContent.OlmEncryptedEventContent(
                mapOf(), Key.Curve25519Key(null, "")
            ),
            UserId("sender", "server")
        )
        olmEncryptionServiceMock.decryptOlm = {
            throw KeyException.KeyNotFoundException("not found")
        }
        cut.handleOlmEvent(event)
        subscriberReceived shouldHaveSize 0
    }
    should("catch OlmLibraryException") {
        val event = ToDeviceEvent(
            EncryptedEventContent.OlmEncryptedEventContent(
                mapOf(), Key.Curve25519Key(null, "")
            ),
            UserId("sender", "server")
        )
        olmEncryptionServiceMock.decryptOlm = {
            throw OlmLibraryException("something")
        }
        cut.handleOlmEvent(event)
        subscriberReceived shouldHaveSize 0
    }
    should("emit decrypted events") {
        val event = ToDeviceEvent(
            EncryptedEventContent.OlmEncryptedEventContent(
                mapOf(), Key.Curve25519Key(null, "")
            ),
            UserId("sender", "server")
        )
        val decryptedEvent = DecryptedOlmEvent(
            TextMessageEventContent("hi"),
            UserId("sender", "server"), keysOf(),
            UserId("receiver", "server"), keysOf()
        )
        olmEncryptionServiceMock.decryptOlm = {
            decryptedEvent
        }
        cut.handleOlmEvent(event)
        subscriberReceived shouldContainExactly listOf(DecryptedOlmEventContainer(event, decryptedEvent))
    }
})